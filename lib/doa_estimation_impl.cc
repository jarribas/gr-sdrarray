/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "libarray.h"
#include "doa_estimation_impl.h"

namespace gr {
  namespace sdrarray {

    doa_estimation::sptr
    doa_estimation::make(int algorithm_type,
    		int samples_per_snapshot,
    		float threshold_db,
    		float carrier_freq_hz,
            int min_Az_deg,
            int max_Az_deg,
            int min_El_deg,
            int max_El_deg)
    {
      return gnuradio::get_initial_sptr
        (new doa_estimation_impl(algorithm_type,
        		samples_per_snapshot,
        		threshold_db,
        		carrier_freq_hz,
        		min_Az_deg,
        		max_Az_deg,
        		min_El_deg,
        		max_El_deg));
    }

    /*
     * The private constructor
     */
    doa_estimation_impl::doa_estimation_impl(int algorithm_type,
    		int samples_per_snapshot,
    		float threshold_db,
    		float carrier_freq_hz,
            int min_Az_deg,
            int max_Az_deg,
            int min_El_deg,
            int max_El_deg)
      : gr::block("doa_estimation",
              gr::io_signature::make(2, 8, sizeof(gr_complex)),
              gr::io_signature::make(2, 2, sizeof(float)))
    {

    	d_algorithm_type=algorithm_type;
    	d_samples_per_snapshot=samples_per_snapshot;
    	d_threshold_db=threshold_db;
    	d_carrier_freq_hz=carrier_freq_hz;

        d_min_Az_deg=min_Az_deg;
        d_max_Az_deg=max_Az_deg;
        d_min_El_deg=min_El_deg;
        d_max_El_deg=max_El_deg;

        const double C_m_s = 299792458.0;  //!< The speed of light, [m/s]

        d_Lambda=C_m_s/d_carrier_freq_hz;

        arma::mat A={{d_Lambda/4, 0, 0},
                     {-d_Lambda/4, 0, 0}};

        d_A=A;

        // DOA message output port
        this->message_port_register_out(pmt::mp("doa"));
    }

    /*
     * Our virtual destructor.
     */
    doa_estimation_impl::~doa_estimation_impl()
    {
    }

    void doa_estimation_impl::forecast (int noutput_items,
            gr_vector_int &ninput_items_required)
    {
        if (noutput_items != 0)
            {
				for(int n=0;n<ninput_items_required.size();n++)
				{
					ninput_items_required[n] = static_cast<int>(d_samples_per_snapshot); //set the required available samples in each call
				}
            }
    }

    int
    doa_estimation_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {

    	//set ouput pointers
    	float *out_doa = (float *) output_items[0];
    	float *out_thr = (float *) output_items[1];


        //build the snapshot matrix
        arma::cx_mat X=arma::cx_mat(arma::zeros(input_items.size(),d_samples_per_snapshot),
        		arma::zeros(input_items.size(),d_samples_per_snapshot));
        for (int ch=0; ch<input_items.size();ch++)
        {
        	//set input pointer for this channel
            const gr_complex *in = (const gr_complex *) input_items[ch];
      	  for (int n=0;n<d_samples_per_snapshot;n++)
      	  {
      		  X(ch,n)=in[n];
      	  }
        }


        // set the amount of samples to be consumed this loop
        unsigned int n_samples_consumed=d_samples_per_snapshot;
        //set the samples to be produced (DOA info)
        int n_samples_produced = 0;

        // compute DOA
        arma::rowvec doa_result;
        switch(d_algorithm_type)
        {

        	case 1: //MUSIC DOA ESTIMATION
        	{
        		arma::cx_mat R_XX=sample_covariance(X); //sample covariance matrix estimation

                // get the eigendecomposition of R; use svd because it sorts eigenvalues
                arma::cx_mat U;
                arma::colvec s;
                arma::cx_mat V;
                arma::svd(U,s,V,R_XX);

                float eig_values_diff_db=10.0*std::log10(arma::max(s)/arma::min(s));

                if (eig_values_diff_db>d_threshold_db)
                {
                    int num_sources=1;
                    arma::cx_mat Gn=U.cols(num_sources, U.n_cols-1); //noise eigenvectors
                    doa_result=doa_music(R_XX, Gn, d_A,
                    		d_carrier_freq_hz,
                    		d_min_Az_deg,
                    		d_max_Az_deg,
                    		d_min_El_deg,
                    		d_max_El_deg);
        			float doa_az_deg=doa_result(1);
        	        //std::cout<<"eig_values_diff_db:"<<eig_values_diff_db<<std::endl;
        			// send asynchronous message to tracking to inform of frame sync and extend correlation time
        			pmt::pmt_t value = pmt::from_float(doa_az_deg);
        			this->message_port_pub(pmt::mp("doa"), value);

        			//produce the doa estimation output
        			out_doa[0]=doa_az_deg;
        			out_thr[0]=eig_values_diff_db;
        			//inform that a new DOA estimation is produced
        	        n_samples_produced= 1;
                }
                break;
        	}
        	case 2: //PHASED ARRAY DOA ESTIMATION
        	{
                  arma::rowvec doa_result;
                  doa_result=doa_phased(X, d_A,
                		d_carrier_freq_hz,
                		d_min_Az_deg,
                		d_max_Az_deg,
                		d_min_El_deg,
                		d_max_El_deg);

                  float eig_values_diff_db=10.0*std::log10(arma::as_scalar(doa_result(0))/arma::as_scalar(doa_result(1)));
                if (eig_values_diff_db>d_threshold_db)
                {
        			float doa_az_deg=doa_result(2);
        			// send asynchronous message to tracking to inform of frame sync and extend correlation time
        			pmt::pmt_t value = pmt::from_float(doa_az_deg);
        			this->message_port_pub(pmt::mp("doa"), value);

        			//produce the doa estimation output
        			out_doa[0]=doa_az_deg;
        			out_thr[0]=eig_values_diff_db;
        			//inform that a new DOA estimation is produced
        	        n_samples_produced= 1;
                }
                break;
        	}
        	default:
        	{
        		break;
        	}
        }

        consume_each(n_samples_consumed);
        return n_samples_produced;

    } /* work */

  } /* namespace sdrarray */
} /* namespace gr */

