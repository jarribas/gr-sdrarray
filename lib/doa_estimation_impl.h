/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_SDRARRAY_DOA_ESTIMATION_IMPL_H
#define INCLUDED_SDRARRAY_DOA_ESTIMATION_IMPL_H

#include <sdrarray/doa_estimation.h>

#include <armadillo>

namespace gr {
  namespace sdrarray {

    class doa_estimation_impl : public doa_estimation
    {
     private:
        int d_algorithm_type;
        int d_samples_per_snapshot;
        float d_threshold_db;
		float d_carrier_freq_hz;
        int d_min_Az_deg;
        int d_max_Az_deg;
        int d_min_El_deg;
        int d_max_El_deg;

        double d_Lambda;

        arma::mat d_A;

     public:
      doa_estimation_impl(int algorithm_type,
    		  int samples_per_snapshot,
    		  float threshold_db,
      		  float carrier_freq_hz,
              int min_Az_deg,
              int max_Az_deg,
              int min_El_deg,
              int max_El_deg);
      ~doa_estimation_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace sdrarray
} // namespace gr

#endif /* INCLUDED_SDRARRAY_DOA_ESTIMATION_IMPL_H */

