#include "libarray.h"
#include <complex>

arma::cx_colvec planar(arma::mat A,double Az,double El,double freq_hz)
{
	//%PLANAR Function to obtain the Steering vector of a PLANAR Isotropic Antenna Array for a narrow band incident wave
	//%INPUT: A -> Matrix of cartesian position coordinates of the antenna elements in rows [X,Y,Z]
	//%       Az -> Azimuth of the incomming wave (degrees) 0 - 360
	//%       El -> Elevation of the incomming wave (degrees) 0 - 90 (Theta)
	//%               90=Broadside, 0= Endfire.
	//%       Freq -> Wave frequency
	//%OUTPUT:
	//%       S -> Steering Vector (phase shifts)

	double wave_speed_m_s=3E8; //%m/s
	double Lambda=wave_speed_m_s/freq_hz;
	double pi=3.141592653589793;
	//wave vector
	double Az_rad=(pi/180.0)*Az;
	double El_rad=(pi/180.0)*El;
	arma::rowvec Ks=arma::zeros(1,3);
	Ks(0)=cos(El_rad)*cos(Az_rad);
	Ks(1)=cos(El_rad)*sin(Az_rad);
	Ks(2)=sin(El_rad);
	Ks=(2*pi/Lambda)*Ks;

	//antenna position vector
	int N_elements=A.n_rows;
	arma::cx_colvec S(arma::zeros(N_elements,1),arma::zeros(N_elements,1));
	for (int n=0;n<N_elements;n++)
	{
		arma::rowvec A_row=A.row(n);
	    S(n)=std::exp(std::complex<double>(0,
			   -arma::as_scalar(Ks*A_row.t()))); //% Scalar product
	}
	return S;
}

arma::cx_mat sample_covariance(arma::cx_mat X)
{
    arma::cx_mat R_XX=X*(X.t())/(double)X.n_cols; //sample covariance matrix estimation
    return R_XX;
}

arma::rowvec doa_music(arma::cx_mat Rxx,
		arma::cx_mat Gn,
		arma::mat A,
		double freq_hz,
		int min_Az_deg,
		int max_Az_deg,
		int min_El_deg,
		int max_El_deg)
{

	double signal_max=0;
	double signal_az=0;
	double signal_el=0;
	for (int Az=min_Az_deg;Az<=max_Az_deg;Az++)
	{
		for (int El=min_El_deg;El<=max_El_deg;El++)
		{
			// Steering vector calculation
			arma::cx_colvec S=planar(A,Az,El,freq_hz); //wave steering vector
			double T_music=1.0/std::fabs(std::real(as_scalar(((S.t())*Gn*(Gn.t())*S)))); // MUSIC test function
			if (T_music>signal_max)
			{
				signal_max=T_music;
				signal_az=Az;
				signal_el=El;
			}
		}
	}
	arma::rowvec result=arma::zeros(1,3);
	result(0)=signal_max;
	result(1)=signal_az;
	result(2)=signal_el;
	return result;
}

arma::rowvec doa_phased(arma::cx_mat X,
		arma::mat A,
		double freq_hz,
		int min_Az_deg,
		int max_Az_deg,
		int min_El_deg,
		int max_El_deg)
{

	double signal_max=0;
	double signal_min=1e6;
	double signal_az=0;
	double signal_el=0;
	for (int Az=min_Az_deg;Az<=max_Az_deg;Az++)
	{
		for (int El=min_El_deg;El<=max_El_deg;El++)
		{
			// Steering vector calculation
			arma::cx_colvec S=planar(A,Az,El,freq_hz); //wave steering vector Nx1
			arma::cx_rowvec y=S.t()*X; //PHASED ARRAY OUTPUT 1xN * NxK =1xK

			double T_phased=arma::as_scalar(y*y.t()).real(); // test function (measured output power)
			if (T_phased>signal_max)
			{
				signal_max=T_phased;
				signal_az=Az;
				signal_el=El;
			}
			if (T_phased<signal_min)
			{
				signal_min=T_phased;
			}
		}
	}
	arma::rowvec result=arma::zeros(1,4);
	result(0)=signal_max/static_cast<double>(X.n_cols);//normalization
	result(1)=signal_min/static_cast<double>(X.n_cols);//normalization
	result(2)=signal_az;
	result(3)=signal_el;
	return result;
}

